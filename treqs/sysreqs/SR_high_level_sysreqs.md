# Meeting Scheduler System

System requirements for a fictitious meeting scheduler system

## High-level requirements

The following text describes high level system requirements

### [requirement id=REQ0001]

The system shall support creating invitations to participants.

### [requirement id=REQ0002 quality=QR0003]

The system shall support creating a meeting event in a personal calendar.

### [requirement id=REQ0003]

The system shall support event labeling and viewing

### [requirement id=REQ0004]

The system shall allow scheduling of resources
